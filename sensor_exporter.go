package main

import (
	"bitbucket.org/futonredemption/embedded"
	"flag"
	"github.com/prometheus/client_golang/prometheus"
	"log"
	"net/http"
	"time"
)

var (
	listenAddressFlag = flag.String("web.listen-address", ":10001", "Address on which to expose metrics and web interface.")
	metricsPathFlag   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
	sampleRateFlag    = flag.Duration("temperature.samplerate", time.Millisecond*500, "Temperature sample rate.")
	sensorTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "sensor_temperature",
		Help: "Current temperature of the room.",
	},
		[]string{"unit"})
)

func init() {
	prometheus.MustRegister(sensorTemperature)
}

func main() {
	flag.Parse()
	//sensor, err := embedded.NewFakeDs1631([]byte{0x00, 0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x05, 0x00, 0x00, 0x00, 0x01, 0x00, 0x05, 0x00})
	sensor, err := embedded.NewDs1631()
	check(err)
	check(sensor.Start())

	keepAlive := make(chan bool)
	ticker := time.NewTicker(*sampleRateFlag)
	go func() {
		for _ = range ticker.C {
			f, err := sensor.GetFahrenheit()
			if err == nil {
				//log.Printf("Temperature: %.3f F", f)
				sensorTemperature.WithLabelValues("f").Set(f)
			} else {
				log.Printf("Error getting temperature: %v", err)
			}
			c, err := sensor.GetCelcius()
			if err == nil {
				//log.Printf("Temperature: %.3f C", c)
				sensorTemperature.WithLabelValues("c").Set(c)
			} else {
				log.Printf("Error getting temperature: %v", err)
			}
		}
	}()
	// Expose the registered metrics via HTTP.
	http.Handle(*metricsPathFlag, prometheus.Handler())
	http.ListenAndServe(*listenAddressFlag, nil)
	<-keepAlive
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
