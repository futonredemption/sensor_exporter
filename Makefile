GO := @GO15VENDOREXPERIMENT=1 go
SOURCE_FILES=$(shell find . -path ./vendor -prune -o -name '*.go' -print)
build: sensor_exporter

lint:
	$(GO) fmt ${SOURCE_FILES}
	$(GO) vet ${SOURCE_FILES}

sensor_exporter:
	$(GO) build sensor_exporter.go

package:
	@snapcraft snap

clean:
	@rm -rf parts/
	@rm -rf snap/
	@rm -rf stage/
	
.PHONY: package build lint clean
